/* Relay Interrupt tester
 *  This code demonstrates how to handle a bouncing connection, i.e. a pushbotton or a relay
 *  
 *  We will turn the on board LED on when the relay is closed, i.e. ON  (or pushbutton is held down)
 *  and turn off the LED when the relay is open, (or pushbutton is released)
 *  
 *  Wiring:
 *  - connect one end of the pushbutton/relay to 5V
 *  - connect the other end to pin 2 (called  RELAY_PIN in the code below).
 *  - connect one end of a 47k resistor to GND and the other end to pin 2 so that the pin will be pulled down when relay is open
 *  
 *  Usage:
 *  - once uploaded to the arduino uno, closing the pushbutton will turn on the led, releasing it will turn off led.
 *  - be sure to display the Serial Monitor
 *  
 *  Notes:
 *  - This version ensures that the ACTION will only be done once per change of state!
 */  

//#define COMPUTE_CRITICAL_SECTION_TIME

#define LED_PIN  (13)
#define RELAY_PIN (2) // must be pin 2 or 3 on an UNO

// we assume the system starts with the relay off! This variable captures the realy state at each change
// this must be volatile since it is manipulated both in the main loop and by interrupts
volatile byte relayState = false;

// ISR for change of relay state
void onChange(){
  relayState = digitalRead(RELAY_PIN);
}

void setup() {
  Serial.begin(115200);
  while(!Serial);
  
  // set up the led pin and turn off the led
  pinMode(LED_PIN,OUTPUT);
  digitalWrite(LED_PIN,LOW);

  // set up the relay interrupt input pin
  pinMode(RELAY_PIN,INPUT);
  attachInterrupt (digitalPinToInterrupt (RELAY_PIN), onChange, CHANGE);  // attach interrupt handler
}

// this could be any action; we just write the LED and display to Serial Montior
void doAction(byte state){
  static unsigned long count = 0;
  static boolean lastState = true;
  boolean bailOut = (lastState == state);  // that means we failed!
  
  digitalWrite(LED_PIN,state); 
  Serial.print("wrote led : ");
  Serial.print(state); 
  Serial.print(bailOut ? " : ERROR" : " : OK");  // if we bail out, then it's an ERROR, there's no other word for it!
  Serial.print(" : clicks : ");
  Serial.println(count++);
  lastState = state;
  while(bailOut);  // halt execution in case of ERROR
}

void loop() {
  static byte lastRelayState = true;  // last state observed
  static byte currentRelayState; // synchronized state variable

  #ifdef COMPUTE_CRITICAL_SECTION_TIME
  unsigned long now = micros();
  #endif
  
  // critical section, ignore relay pin changes here; NOTE If there is a change on the relay in this section, it will be lost, so we keep it very short!
  detachInterrupt (digitalPinToInterrupt (RELAY_PIN));
  currentRelayState = relayState;
  attachInterrupt (digitalPinToInterrupt (RELAY_PIN), onChange, CHANGE);
  // end of critical section
  #ifdef COMPUTE_CRITICAL_SECTION_TIME
  Serial.print("Critical section duration: ");
  Serial.print(micros() - now);
  Serial.println(" µs");
  #endif
  
  if(lastRelayState!=currentRelayState){ // if the state has changed since last cycle : stop interrupt,reset last state and do action,   
    lastRelayState = currentRelayState;
    doAction(lastRelayState); // NOTE: if this actions takes a long time then changes in the relay state may be lost!
  }
  
}
