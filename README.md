# Relay Tester

# Problem
We have a bouncy switch that must be monitored so that at each genuine change, not bounce, the state is used to command an action.

The bounces need to be ignored such that only a true held state is taken into account.

However, frequent changes in state must be accepted and taken into account.

# More abstractly

We have bursts of asyncrhonous actions coming from the relay. Each burst represents a single valid action.
The direction of the action is determined by the first element of the burst.
So, if we see LOW HIGH LOW HIGH LOW we need to read LOW only.

Once we'vre read a first element of the burst, we need to ignore any further elements.

On the other hand, we are only interested in actions which alternate HIGH then LOW then HIGH then LOW etc.
* if we capture a HIGH state after a valid LOW state, we act.
* if we capture a LOW state after a valid HIGH state, we act.

But what if the state of the relay changes during the action?
* if we mask state changes during the action,  then we could miss a very short state change. But our overal state remains coherent 
* If we allow state changes during the action, then a very short state change could render our overal state incoherent.

So, we must create a critical section where there can be no asynchronous state change. Inside the critical section,
we capture the asynchronous state in a synchronized variable. Thus the critical section is minimized and the chance of missing a state
change is minimal.

# Solution
## Hardware
The relay input pin must be pulled down to prevent wild floating.

## SW architecture
### ISR
* An Interrupt Service Routine is set to capture any change of state on the relay pin.
* the ISR is triggered by any change. In the ISR we are masked of other interrupts. There, the relay pin is read and its value is assigned to an asynchronous variable relayState.

### Main Loop
* The last relay state is maintained in static local (synchronized) variable lastRelayState
* A critical section is created by disabling the ISR.
* In the critical section, i.e. while the asynchronous variable relayState cannot change, a syncrhonized local variable currentRelayState is set to the value of relayState.
* The critical section ends with the re-enabling of the ISR. From this point, only the local syncrhonized variable currentRelayState is used in the main loop.
* In the rest of the main loop, we check to see if the currentRelayState is different from the lastRelayState. if so, we update lastRelayState, and use it as input to the 
doAction function.

## Issues
* If there are changes to the relay's state during the critical section they will be lost, ie. relayState will not be updated.
* If there are an even number of changes to the asynchronous variable relayState they will not be handled by the program as it stands.

The solution and known issues lead to a domain of application.

## Domain of Application
* The frequency of changes in state of the relay must be low enough to allow the main loop time to execute doAction for each change.
* The duration of a state must be sufficient to allow the critical section to exit. Given that the critical section is only 3 lines of code, that minimum duration is very small. 
On the Arduiono Uno, the critical second duration is 2.3ms. If we allow for a factor of 2 margin, this code could handle a frequency of around 200Hz.

## Testing
* A demo version of doAction switches the on board led to the current state and displays that state on the Serial Monitor with a check appended, 
i.e. if the state is as expected it is OK, if not it is an ERROR.
* In manual testing clicking a tactile pushbutton as quickly as possible, the program works properly. 

